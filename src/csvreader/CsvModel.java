package csvreader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * This Class Reads in the File and adds the elements of the user specified
 * column into an ArrayList It then Runs Calculations on them
 *
 * @author Patrick Macken
 */
public class CsvModel {

    
    static private double avg = 0.0;
    static private double stDev = 0.0;
    private double sum = 0.0;
    private ArrayList<Double> csvVals = new ArrayList<>();
    

   static public double getDev() {
        
        return stDev;
    }

    static public double getAvg() {
        
        return avg;

    }
    public void setAvg(double average){
        avg = average;
    }
    public void setDev(double deviation ){
        stDev = deviation;
    }

    /**
     * Reads in the specified CSV file and Column Number Adds the specified
     * column number to the csvVals ArrayList Then calls List Iterator to sum up
     * the column Then calls Std Deviation to calculate the Standard Deviation
     *
     * @param theFile The user specified file to be read in.
     * @param colNum Which column from the CSV the user wants tabulated
     */
    public void readFile(String theFile, String colNum) {
        String line = "";
        int cols = Integer.parseInt(colNum); //parsing the user entered column number into an int

        try {
            int k = 0;
            BufferedReader br = new BufferedReader(new FileReader(theFile));
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if (k == 0) {
                    k++;
                    continue;
                }
                double currDbl = Double.parseDouble(values[cols]); //Parsing the String to a double
                csvVals.add(currDbl); //adding to the arrayList 
            }
            runCalculations(); //runs all the calculations 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * This Runs all of the functions that calculate the Sum, Average, and
     * Standard Deviation called in readFile()
     */
    public void runCalculations() {
        listSummation();
        avgCalc();
        calcStdDev();
    }

    /**
     * Sums up the specified column by iterating thru the list and adding each
     * element to tempsum.
     *
     * @return The Computed Average for the specified column
     */
    public void listSummation() {
        double tempSum = 0.0;
        for (Double d : csvVals) {
            tempSum += d;
        }
       
        sum = tempSum;
        
    }

    /**
     * Computes the Average by taking the sum taken in from listIterator() and
     * dividing by the list size.
     *
     * @param d the sum from the list
     * @return The Average
     */
    public void avgCalc() {
        double tempAvg = sum;
        tempAvg /= csvVals.size();
        setAvg(tempAvg);
       
    }

    /**
     * Calculates Standard Deviation via published formula
     */
    public void calcStdDev() {
        double tempStdDev = 0.0;
        for (Double d : csvVals) {
            tempStdDev = tempStdDev + Math.pow((d - avg), 2);

        }
        double e = tempStdDev / csvVals.size();
        double f = Math.sqrt(e);
        setDev(f);
       
    }

}
