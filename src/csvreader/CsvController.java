package csvreader;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import java.text.DecimalFormat;
/**
 *
 * @author Pat
 */
/**
 * The Controller Class for the GUI application 
 *
 * @author Pat
 */
public class CsvController implements ActionListener {

    
    private final JTextField fileName;
    private final JTextField colField;
    private  CsvModel io = new CsvModel();
    private final JTextField theAvg;
    private final JTextField theStdDev;
    /**
     * 
     * @param avg the Avg Text field where column average is printed
     * @param stDevThe The Standard Deviation field where this value is printed 
     * @param fName The filename input by the user 
     * @param colF the Column field input by the user 
     */
    public CsvController (JTextField avg, JTextField stDev, JTextField fName, JTextField colF){
        this.theAvg = avg;
        this.theStdDev = stDev; 
        this.fileName = fName;
        this .colField = colF; 
    }
    /**
     * Calls readFile from FileIO()
     */
    public void readFile(String file, String cols) {
       io.readFile(file, cols);
        
    }

    /**
     * Prints the Average and Standard Deviation from the user specified column
     * sends the data to the GUI for display
     */
    public void printResults() {
        double avg =io.getAvg(); 
        double dev = io.getDev();
        DecimalFormat df = new DecimalFormat("###.##");
        this.theAvg.setText(df.format(avg));
        this.theStdDev.setText(df.format(dev));
    }
    
    /**
     * This gets called when the user clicks "Run Calculations in the GUI
     * Retrieves the FileName and the Column # the user entered then calls Readfile() and printResults()
     * @param ae The Action 
     */
    @Override 
    public void actionPerformed(ActionEvent ae){
        if(ae.getActionCommand().equals("Run Calculations")){
            String file = this.fileName.getText();
            String colNum = this.colField.getText();
            readFile(file,colNum);
            printResults();
    }
    }
    
}
