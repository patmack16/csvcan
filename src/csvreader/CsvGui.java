/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csvreader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
/**
 *The GUI class for the Application, handles displaying/layout of all the elements 
 * @author Pat
 */
public class CsvGui extends JFrame {
    private final JLabel avgLabel = new JLabel ("Column Average was: ");
    private final JLabel stdDevLabel =  new JLabel ("Standard Deviation was: ");
    private final JLabel fileLabel = new JLabel ("Enter FileName Here: ");
    private final JLabel colLabel = new JLabel ("Enter Column # here: ");
    private final JButton runCalculations = new JButton("Run Calculations");
    private final JTextField theAvg = new JTextField(20);
    private final JTextField theStdDev  = new JTextField(20);
    private final JTextField fileName = new JTextField(20);
    private final JTextField colField = new JTextField(20);
    
    private final CsvController control = new CsvController (theAvg, theStdDev, fileName, colField);
    /**
     * Constructor that sets up all of the elements and their respective locations 
     */
    public CsvGui(){
        super("Calculations");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        
        this.setSize(700,500);
        this.getContentPane().setLayout(null);
        
        this.fileLabel.setBounds( 20, 20,150, 30);
        this.getContentPane().add( this.fileLabel);
        
        this.fileName.setBounds(200, 20, 80, 30);
        this.getContentPane().add(fileName);
        
        this.colLabel.setBounds (400, 20, 150, 30);
        this.getContentPane().add( this.colLabel);
        
        this.colField.setBounds(550,20,60,30);
        this.getContentPane().add(colField);
        
        this.avgLabel.setBounds(20,360,125,30);
        this.getContentPane().add( this.avgLabel);
        
        this.theAvg.setBounds(150,360,125,30);
        this.getContentPane().add(this.theAvg);
        
        this.stdDevLabel.setBounds(300,360,150,30);
        this.getContentPane().add( this.stdDevLabel);
        
        this.theStdDev.setBounds(500,360,150,30);
        this.getContentPane().add(this.theStdDev);
        
        this.runCalculations.setBounds( 20, 120, 120, 30);
        this.runCalculations.addActionListener(control);
        
        this.getContentPane().add( this.runCalculations);
        
    }
        
    }
    

